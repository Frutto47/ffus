'''Standart texture types, that will be used for every material creatiom:
    These values will be keys in below defined dicts:
    ['Diffuse', 'ORM', 'Normal', 'Alpha', 'BDE']
    '''


# be sure to include desired texture type in this list, otherwise it won't be created
all_ordered_texture_types = [ # common
                        'Diffuse', 'ORM', 'Normal', 'Alpha', 'Diffuse + Alpha',
                        # DBD
                        'BDE',
                        # TCSM
                        'SRTDM' # SRTDM stands for Specular Roughness Transluceny (Scattering) Detail Mask
                        ]


possible_dbd_texture_types = {  'Diffuse': ['Diffuse',
                                            'BaseColor Map',
                                            'BaseColorTexture',
                                            'Main_BaseColor'],
                                'ORM': ['AORoughnessMetallic',
                                        'PM_SpecularMasks',
                                        'Main_ORM'],
                                'Normal': ['Normal',
                                        'NormalMap Texture',
                                        'Normal Map',
                                        'NormalTexture',
                                        'Main_Normal'],
                                'Alpha': ['Alpha_Mask',
                                        'MaskFromOtherPart',
                                        'Alpha mask',
                                        'Opacity Mask Texture'],
                                'BDE': ['Blood Dirt Emissive']
                            }


possible_tcsm_texture_types = {
                                'Diffuse': ['Diffuse(RGB)Occlusion(A)',
                                             'Diffuse(RGB)Opacity(AOptional)',
                                             'IrisColor'],
                                'ORM': ['Occlusion(R)Rough(G)Metallic(B)'],
                                'SRTDM': ['SRTdMTexture'],
                                'Normal': ['NormalMapTexture',
                                            'NormalMap',
                                            'Normal'],
                                'Alpha': ['Alpha_Mask',
                                         'MaskFromOtherPart',
                                         'Alpha'],
                                'Diffuse + Alpha': ['RGB: Diffuse A:Opacity'],
                                'Depth': ['Depth']
                            }
                            
filepaths_in_folder_dict = {}
