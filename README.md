# FFUS - Frutto Fast Unreal Setup

![](https://img.shields.io/badge/Blender%20version-4.3.2-F5792A?style=for-the-badge&logo=blender)
![](https://img.shields.io/badge/Python%20version-3.11.9-FFE872?style=for-the-badge&logo=python)
![](https://img.shields.io/badge/FFUS%20version-1.3.4-9BE737?style=for-the-badge)

## Description
#### FFUS is an addon for blender, that allows you to automate the process of setting up material shaders (node-trees) for 3D models from games based on Unreal Engine.

### Features

1. One-click material setup.
2. You may choose to set up only active material you need
3. Support of both Umodel and Fmodel methods of exporting 3D models from the games.
4. Games tested on:

    1. Dead by daylight
    2. The Texas Chain Saw Massacre

## Installation
Install easy as a simple blender add-on.

1. Go to Releases on the right side of this page -> Download the latest release of FFUS vX.X.X (zip file attached at release notes section).
2. Blender -> Edit -> Preferences -> Addons -> Install from Disk.
3. Select .zip file, press install.
4. Tick a checkbox next to the addon to activate it.
5. FFUS tab should now appear in N panel of 3D Viewport.

## Usage 

### FFUS UI:

![image not loaded](<readme images/01_FFUS_UI.png> "FFUS UI")
FFUS UI is very simple and consists of:

1. Material setup buttons.
2. Settings panel.
3. Debug panel.
4. Info panel.

Each panel has its own purpose. 

+ Material setup buttons are used for setting up materials in one click, after you have configured all necessary settings in settings panel.
+ Settings panel is where you set all parameters, needed for setting up your materials.
+ Debug panel is your friend, when something weird occurs.
+ Info panel provides links to Discord servers to ask questions and report issues about FFUS.

### Step-by-step guide
#### Let's dive a little bit deeper and see the workflow with FFUS addon 
Import the model you want to set up materials to, I will use Meg Thomas from Dead by Daylight in this example:

![image not loaded](<readme images/02_FFUS_demonstration.png> "FFUS workflow")


1. Select all mesh objects you want to set up materials to. FFUS will only add materials to selected meshes.
2. Go to settings panel and navigate to a path of an Export folder, where you've exported game files. This is **very** important. It **must** be the exact same path, not a parent or a child. Example:

   ![image not loaded](<readme images/03_FFUS_export_path.png> "FFUS export path settings")
3. Select the game that are your files from. Currently, there are **Dead by Daylight** and **Texas Chain Saw Massacre**.
4. Select export program you used: **Umodel** or **Fmodel**.
5. You can choose what you want to do with mesh parts, that have unused materials assigned (these materials are usually used for physics simulations or any other purposes of Unreal Engine and don't have any textures applied on them. You can see them as pure white parts on some of the characters clothes). So you have three options:

    1. **Delete**: all faces of the mesh with unused material assigned on them will be deleted. 
    2. **Hide**: material will be hidden with alpha parameter set to 1 in its Principled BSDF material node, so it won't be visible in both material and render blender preview modes.
    3. **Do nothing**: unused materials are left as they are.

6. Click setup materials button and FFUS will all add textures to all materials and create the corresponding shader node setup.

#### And we have our result: 
![image not loaded](<readme images/04_FFUS_result.png> "FFUS result")

### How to debug
If you see blender's blue info message at the bottom (just how in my example), containing info about how much materials were set up and how much time it took, then setup process finished without any issues. But sometimes you might see a yellow warning like that:

![image not loaded](<readme images/5_FFUS_warnings.png> 'FFUS warnings')

Here is where debug panel comes in handy, just go to debug panel and press "Generate log file (.json)"
Then you should provide a path to a folder, where debug file will be saved.

![image not loaded](<readme images/06_FFUS_debug_enable.png> 'debug enable')

 Debug .json file looks like that, it contains info about FFUS settings and materials and textures, it was able or wasn't able to find.

#### FFUS debug file example:
![image not loaded](<readme images/07_FFUS_debug_file_example.png> 'Debug file example')

### FFUS debug missing textures example

If some textures or material files were not found, log file will let you see it in corresponding section:

![image not loaded](<readme images/08_FFUS_debug_missing_textures.png> 'Debug missing textures')

Here you can see, FFUS hasn't found Normal map texture for "MI_MTHead00" material in "Outfit00" folder, where it supposed to be present. With this info, you can quickly understand what's the problem and how to fix it.

### FFUS Preferences

FFUS has Preferences in addon view (Edit -> Preferences -> Add-ons) you can set up your default export path, which you usually use to export game files (If you do it, every time you open blender, this path will be already installed as export path parameter, and you don't need to manually configure it) and also from version 1.3.1 you are able to check for updates.

![image not loaded](<readme images/FFUS_addon_prefs.png> "FFUS addon preferences")

### FFUS Setting up only one material

Since FFUS version 1.3.3 you may choose to set up only one material for set up (instead of setting up all materials of the selected object at once, it was default behaviour). This feature is mostly useful for saving up you time, when you need to 'resetup' only one or several materials, not all materials of the model. It is also possible to clear one material as well.

![image not loaded](<readme images/09_FFUS_set_up_only_one_material.png> "FFUS settings preset feature")

### FFUS Presets

Sence FFUS version 1.3.4 "settings preset" feature has beed implemented. You can now easily save your desired settings as a preset, that you can later set up in a couple of clicks. This is mostly useful for storing 'export path' parameter, which often takes some time to manually navigate and find needed folder. 

![image not loaded](<readme images/10_FFUS_settings_presets.png> "FFUS settings preset feature")

## Notes

1. This addon is written for blender version 4.0 and above. You can also use an alternative to FFUS [Anime Nyan UEShader Script](https://github.com/AnimNyan/UEShaderScript), but it supports blender versions only below 4.0.

