import time
from datetime import datetime
import json
from pathlib import Path

from ..functions.common_functions import *
from ..functions.operators_help_funcs import *
from ..functions.skipped_materials import *


class FFUS_OT_setup_materials(bpy.types.Operator):
    """Setup Frutto Fast Unreal Materials"""
    bl_idname = 'object.setup_materials'
    bl_label = 'Set Up All Materials'
    bl_options = {'REGISTER', 'UNDO', 'PRESET'}


    @classmethod
    def poll(self, context):
        props = context.scene.ffus_props
        if not context.selected_objects:
            return False
        elif not is_material_present(context):
            return False
        elif props.setup_only_active_material and not is_active_material_present(context):
            return False
        else:
            export_path = Path(context.preferences.addons['FFUS'].preferences.export_path)
            if export_path.is_absolute() and export_path.exists():
                return True
        return False


    def execute(self, context):
        # Start measuring execution time
        start_time = time.time()
        # Logging flag
        log_enabled = False

        props = context.scene.ffus_props
        addon_prefs = context.preferences.addons['FFUS'].preferences
        
        if props.generate_log_file \
                and Path(addon_prefs.log_folder_path).is_absolute() \
                and Path(addon_prefs.log_folder_path).exists():
            # log_time is used to create log filename with timestamp
            log_time = datetime.now()

            # cerating data dict for writing in a log .json file
            log_data = {'Export folder path': addon_prefs.export_path,
                    'Export program': props.export_program,
                    'Game name': props.game_name,
                    'Action on unused materials': props.action_on_unused_materials,
                    'Log folder path': addon_prefs.log_folder_path,
                    }
            log_enabled = True

        materials_to_setup = set()
        materials_to_skip = set()
        
        if props.setup_only_active_material:
            materials_to_setup.add(context.active_object.active_material)
        else:
            materials_to_setup, materials_to_skip = define_materials(context)
        
        
        # mat_filepath_dict: key - material, value - path to text file
        # mat_filepath_dict guarantees, that path to material text file exists
        mat_filepath_dict = create_mat_filepath_dict(props, addon_prefs.export_path, materials_to_setup)

        if log_enabled:
            log_data['Materials to setup'] = [mat.name for mat in materials_to_setup]
            log_data['Materials to skip'] = [mat.name for mat in materials_to_skip]
            log_data['Material name: Material text file path'] = {key.name : str(value) for key, value in mat_filepath_dict.items()}
            log_data['Material name: Textures found'] = dict()

        # define possible textures for creating texture_type_path_dict
        if props.game_name == 'DBD':
            from ..common_variables import possible_dbd_texture_types as possible_textures
        elif props.game_name == 'TCSM':
            from ..common_variables import possible_tcsm_texture_types as possible_textures


        textures_not_found_dict = dict()
        # warn about missing textures flag
        warn_missing_textures = False

        # materials setup main cycle
        for mat, mat_filepath in mat_filepath_dict.items():
            # texture_type_path_dict - dict with keys - texture_types, values - paths to according textures
            texture_type_path_dict, textures_not_found_dict = create_texture_type_path_dict(props, mat_filepath, Path(addon_prefs.export_path), possible_textures)

            if log_enabled:
                log_data['Material name: Textures found'][mat.name] = texture_type_path_dict
                if textures_not_found_dict:
                    log_data['Material name: Textures not found'] = {mat.name: {texture_name: str(path) for texture_name, path in textures_not_found_dict.items()}}

            if textures_not_found_dict:
                warn_missing_textures = True

            create_empty_principled_bsdf_setup(mat)
            create_material_setup(props, mat, texture_type_path_dict)

        
        # operating on skipped materials
        if materials_to_skip:
            if props.action_on_unused_materials == 'DELETE':
                for mat in materials_to_skip:
                    delete_unused_material(mat)
            elif props.action_on_unused_materials == 'HIDE':
                for mat in materials_to_skip:
                    hide_unused_material(mat)


        if log_enabled:
            # create log json file and write log_data dict to ita
            log_filepath = addon_prefs.log_folder_path + '/FFUS_log ' + log_time.strftime("%Y-%b-%d-%H_%M_%S") + '.json'
            with open(log_filepath, 'w', encoding='utf-8') as log_file:
                json.dump(log_data, log_file, ensure_ascii=False, indent=4)

        if warn_missing_textures:
            self.report({'WARNING'}, 'Some textures might be missing! Check log file for additional info.')
            return {"FINISHED"}
        if len(materials_to_setup) != len(mat_filepath_dict):
            self.report({'WARNING'}, f"{len(materials_to_setup) - len(mat_filepath_dict)} material(s) file(s) not found, some textures might be missing! Check log file for additional info.")
            return {"FINISHED"}
        self.report({'INFO'}, f"{len(mat_filepath_dict)} material(s) setup in {time.time() - start_time:.2} seconds, materials skipped: {len(materials_to_skip)}")
        return {"FINISHED"}


class FFUS_OT_clear_materials(bpy.types.Operator):
    """Clear Frutto Fast Unreal Materials"""
    bl_idname = 'object.clear_materials'
    bl_label = 'Clear Materials'
    bl_options = {'REGISTER', 'UNDO'}


    @classmethod
    def poll(self, context):
        props = context.scene.ffus_props
        if not context.selected_objects:
            return False
        elif not is_material_present(context):
            return False
        elif props.clear_only_active_material and not is_active_material_present(context):
            return False
        return True


    def execute(self, context):
        props = context.scene.ffus_props

        # define materials to operate on
        materials_to_clear = set()

        if props.clear_only_active_material:
            materials_to_clear.add(context.active_object.active_material)
        else:
            for obj in context.selected_objects:
                for mat_slot in obj.material_slots:
                    # skip empty slot
                    if not mat_slot.name:
                        continue
                    materials_to_clear.add(mat_slot.material)

        for mat in materials_to_clear:
            # press 'Use Nodes' button
            mat.use_nodes = True

            # clear all nodes in materail node tree
            mat.node_tree.nodes.clear()

            create_empty_principled_bsdf_setup(mat)
        
        # delete unused textures from blend file
        bpy.ops.outliner.orphans_purge(do_recursive=True)
        return {"FINISHED"}


classes = [
    FFUS_OT_setup_materials,
    FFUS_OT_clear_materials,
    ]

def register():
    for cl in classes:
        bpy.utils.register_class(cl)


def unregister():
    for cl in classes:
        bpy.utils.unregister_class(cl)
