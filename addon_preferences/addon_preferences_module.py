import bpy
from bpy.types import AddonPreferences
from bpy.props import StringProperty

from .. import addon_updater_ops

@addon_updater_ops.make_annotations
class FFUS_addon_preferences(AddonPreferences):
    # this must match the add-on name, use '__package__'
    # when defining this in a submodule of a python package.
    
    # bl_idname = 'name of the folder with addon'
    bl_idname = 'FFUS'


    # Addon updater preferences
    auto_check_update = bpy.props.BoolProperty(
	  name="Auto-check for Update",
		description="If enabled, auto-check for updates using an interval",
		default=False)

    updater_interval_months = bpy.props.IntProperty(
		name='Months',
		description="Number of months between checking for updates",
		default=0,
		min=0)

    updater_interval_days = bpy.props.IntProperty(
		name='Days',
		description="Number of days between checking for updates",
		default=7,
		min=0,
		max=31)

    updater_interval_hours = bpy.props.IntProperty(
		name='Hours',
		description="Number of hours between checking for updates",
		default=0,
		min=0,
		max=23)

    updater_interval_minutes = bpy.props.IntProperty(
		name='Minutes',
		description="Number of minutes between checking for updates",
		default=0,
		min=0,
		max=59)


    
    
    export_path: StringProperty(
        name='Export path',
        subtype='DIR_PATH',
        description='Path to folder, where export programs output all files',
        default='',
    )

        
    log_folder_path: StringProperty(
        name='Log folder path',
        subtype='DIR_PATH',
        description='Path to folder, where log .json file will be saved',
        default='',
    )
    

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'export_path')

        # addon updater ui function call
        addon_updater_ops.update_settings_ui(self, context)


def register():
    bpy.utils.register_class(FFUS_addon_preferences)


def unregister():
    bpy.utils.unregister_class(FFUS_addon_preferences)
