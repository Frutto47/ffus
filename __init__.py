from bpy.props import PointerProperty
from bpy.types import Scene
from bpy.utils import register_class, unregister_class

# integration with addon updater
from . import addon_updater_ops

bl_info = {
    'name': "Frutto Fast Unreal Setup (FFUS)",
    'blender': (4, 3, 2),
    'category': 'Object',
    "author": "Frutto",
    'version': (1, 3, 4),
    "location": "3D View > N-panel > FFUS Tab",
    "description": "Setup your Unreal Engine materials in one click",
    "doc_url": "https://gitlab.com/Frutto47/ffus",
    'support': 'COMMUNITY',
    'show_expanded': True
}

# Set ffus_version as an attrubute for addon_preferences_module
# this is necessary for correct text display of ffus version in UI panel
# if addon version has been changed, we just change it in bl_info dict above
# and all text labels for ffus version are changed automatically
from .addon_preferences import addon_preferences_module


addon_preferences_module.ffus_version = bl_info['version']


def register():
    # register addon updater
    addon_updater_ops.register(bl_info)


    from .addon_preferences import addon_preferences_module
    addon_preferences_module.register()

    from .operator_presets import presets
    presets.register()

    from .panels import panels_module
    panels_module.register()

    from .operators import operators_module
    operators_module.register()

    from .properties import properties_module
    properties_module.register()

    Scene.ffus_props = PointerProperty(type=properties_module.FFUS_props)
    

def unregister():
    addon_updater_ops.unregister()

    from .addon_preferences import addon_preferences_module
    addon_preferences_module.unregister()

    from .operator_presets import presets
    presets.unregister()

    from .panels import panels_module
    panels_module.unregister()

    from .operators import operators_module
    operators_module.unregister()

    from .properties import properties_module
    properties_module.unregister()

    del Scene.ffus_props
