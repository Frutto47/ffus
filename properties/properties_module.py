import bpy

from bpy.types import PropertyGroup
from bpy.props import FloatProperty, IntProperty, StringProperty, BoolProperty, EnumProperty


class FFUS_props(PropertyGroup):
    game_name: EnumProperty(
        items=[ ('DBD', 'Dead by Daylight', 'Dead by daylight game'),
                ('TCSM', 'Texas Chain Saw Massacre', 'Texas Chain Saw Massacre game')
                ],
        name='Select game',
        description='Select game from which to setup materials',
        default='DBD',
        )

    export_program: EnumProperty(
        items=[ ('FMODEL', 'Fmodel', 'Select if files were exported with Fmodel'),
                ('UMODEL', 'Umodel', 'Select if files were exported with Umodel'),
                ],
        name='Export method',
        description='Select files export method',
        default='FMODEL',
        )

    action_on_unused_materials: EnumProperty(
        items=[ ('DO_NOTHING', 'Do nothing', 'Do nothing on mesh with unused materials'),
                ('HIDE', 'Hide', 'Hide mesh parts with unused materials'),
                ('DELETE', 'Delete', 'Delete mesh parts with unused materials')                
                ],
        name='Mesh with unused materials',
        description='Select what do you want to do with mesh parts with unused material',
        default='DO_NOTHING',
        )

    generate_log_file: BoolProperty(
        name='Generate log file (.json)',
        description='Generate log file in .json format for each "setup_materials()" operator call',
        default=False,
    )
    setup_only_active_material: BoolProperty(
        name='Set up only active material',
        description='Set up material only on active material (which is selected in materials list)',
        default=False,
    )
    clear_only_active_material: BoolProperty(
        name='Clear only active material',
        description='Clear only active material (which is selected in materials list)',
        default=False,
    )


def register():
    bpy.utils.register_class(FFUS_props)


def unregister():
    bpy.utils.unregister_class(FFUS_props)
    