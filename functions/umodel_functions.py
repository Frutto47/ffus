from pathlib import Path


def create_mat_filepath_umodel_dict(export_path: str, materials_to_setup: set) -> dict:
    mat_filepath_dict = dict()
    # find all filepaths in export path with .props.txt ending and matching materials names
    for filepath in Path(export_path).glob('**/*'):
        if not filepath.is_file():
            continue
        if not filepath.name.endswith('.props.txt'):
            continue
        
        for material in materials_to_setup:
            if str(filepath.name).split('.')[0] == material.name:
                mat_filepath_dict[material] = filepath
    return mat_filepath_dict


def create_texture_type_path_umodel_dict(mat_filepath: Path, export_path: Path, possible_textures: dict) -> (dict, dict):
    with open(mat_filepath) as file:
        lines = [line.strip() for line in file.readlines()]

    # define texture_type_path_dict dict
    # key - texture type, that was found in props.txt file
    # value - absolute path to the texture
    texture_type_path_dict = dict()

    # textures not found dict
    textures_not_found = dict()

        
    possible_textures_in_file = []
    for texture_type in possible_textures:
        for texture_name in possible_textures[texture_type]:
            possible_textures_in_file.append(f'{{ Name={texture_name} }}')


    for i, line in enumerate(lines):
        # check if encountered any texture from possible_textures_in_file list if not, then continue
        props_txt_texture_type = [tex for tex in possible_textures_in_file if tex in line]
        if not any(props_txt_texture_type):
            continue
        
        # determine the type of the texture encountered
        # from { Name=Diffuse }
        # to Diffuse
        props_txt_texture_type = props_txt_texture_type[0][7:-2]


        # this is needed to make texture_paths dict with a key of common texture
        # so, if for example we encountered { Name=AORoughnessMetallic } texture, 
        # it will be transformed to texture_paths['ORM']
        # and not texture_type_path_dict['AORoughnessMetallic']
        for texture_type in possible_textures:
            if props_txt_texture_type in possible_textures[texture_type]:
                current_texture_type = texture_type
                break

        # from ParameterValue = Texture2D'/Game/Characters/Campers/Meg/Textures/Outfit008/T_MTAcc008_BC.T_MTAcc008_BC'
        # to Game/Characters/Campers/Meg/Textures/Outfit008/T_MTAcc008_BC
        relative_path_to_texture = Path(lines[i + 1].split("'")[1].split('.')[0][1:])

        texture_name = relative_path_to_texture.stem
        folder_with_texture = export_path / relative_path_to_texture.parent

        from .common_functions import get_filepaths_in_folder
        from ..common_variables import filepaths_in_folder_dict


        filepaths_in_folder_dict[str(folder_with_texture)]: list = filepaths_in_folder_dict.get(str(folder_with_texture), get_filepaths_in_folder(folder_with_texture))

        for filepath in filepaths_in_folder_dict[str(folder_with_texture)]:
            if texture_name == filepath.stem:
                # texture file was found
                texture_type_path_dict[current_texture_type] = str(filepath)
                break
        
        
        if not texture_type_path_dict.get(current_texture_type):
            textures_not_found[f'{texture_type} {texture_name}'] = folder_with_texture
    return texture_type_path_dict, textures_not_found
