import bpy
from .fmodel_functions import *
from .umodel_functions import *
from .material_creation import *
from .tcsm_material_creation import *
from ..common_variables import all_ordered_texture_types

# noinspection PyUnresolvedReferences
from ..addon_preferences.addon_preferences_module import ffus_version

from .image_functions import load_image_to_blender

from bpy.types import Material


def ffus_ver_to_string(ffus_version: tuple=ffus_version) -> str:
    if len(ffus_version) == 1:
        return ffus_version[0]
    return '.'.join([str(i) for i in list(ffus_version)])


def create_mat_filepath_dict(props, export_path, materials_to_setup: set) -> dict:
    if props.export_program == 'UMODEL':
        return create_mat_filepath_umodel_dict(export_path, materials_to_setup)
    elif props.export_program == 'FMODEL':
        return create_mat_filepath_fmodel_dict(export_path, materials_to_setup)


def create_texture_type_path_dict(props, mat_filepath: Path, export_path: Path, possible_textures: dict) -> dict:
    if props.export_program == 'UMODEL':
        return create_texture_type_path_umodel_dict(mat_filepath, export_path, possible_textures)
    elif props.export_program == 'FMODEL':
        return create_texture_type_path_fmodel_dict(mat_filepath, export_path, possible_textures)


def get_filepaths_in_folder(folder: Path) -> list:
    if not folder.exists():
        return []
    return [filepath for filepath in folder.iterdir() if filepath.is_file()]


def create_material_setup(props, mat: Material, textures_filepaths: dict) -> None:
    # sort textures_paths dict keys in that order: Diffuse -> Alpha -> ORM -> Normal -> BDE
    textures_filepaths = {item: textures_filepaths[item] for item in all_ordered_texture_types if item in textures_filepaths}

    for texture_type, texture_path in textures_filepaths.items():
        image = load_image_to_blender(texture_path)

        # common setups: Diffuse, ORM, Normal, Alpha
        if texture_type == 'Diffuse':
            create_bc_setup(mat, image)
        elif texture_type == 'ORM':
            create_orm_setup(mat, image)
        elif texture_type == 'Normal':
            create_normal_map_setup(mat, image)
        elif texture_type == 'Alpha':
            create_alpha_setup(mat, image)

        # DBD Blood Dirt Emissive texture setup
        elif texture_type == 'BDE':
            create_bde_setup(mat, image)

        elif texture_type == 'Diffuse + Alpha':
            create_diffuse_alpha_map_setup(mat, image)

        # TCSM SRTDM texture setup
        elif texture_type == 'SRTDM':
            create_srtdm_setup(mat, image)

        else:
            print(f'Unexpected texture type: {texture_type} in Material: {mat.name}')

        
    # DBD hair and lashed handling
    # change material settings to alpha hashed or alpha clip in eevee settings, depending on material name
    if props.game_name == 'DBD':
        if 'hair' in mat.name.lower():
            mat.blend_method = 'HASHED'
        elif 'lashes' in mat.name.lower():
            mat.blend_method = 'CLIP'
        elif 'Alpha Map texture' in [node.name for node in mat.node_tree.nodes]:
            mat.blend_method = 'CLIP'
    
    # TCSM materials handling
    elif props.game_name == 'TCSM':
        if 'lashes' in mat.name.lower():
            # add alpha texture, if it was not added in alpha setup (e.g. for Connie)
            if 'Alpha Map texture' not in [node.name for node in mat.node_tree.nodes]:
                create_tcsm_lashes_setup(mat)
 
        # eyes materials handling
        elif 'eyeocclusion' in mat.name.lower():
            create_tcsm_eye_occlusion_setup(mat)

        elif 'eyetransition' in mat.name.lower():
            create_tcsm_eye_transition_setup(mat)

        elif 'eyeiris' in mat.name.lower():
            create_tcsm_eye_iris_setup(mat, iris_bc_texture_path=textures_filepaths['Diffuse'])
