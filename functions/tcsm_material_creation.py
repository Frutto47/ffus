import bpy
from bpy.types import Material

from pathlib import Path


from .image_functions import load_image_to_blender, load_image_to_node
from .material_creation import (create_empty_principled_bsdf_setup,
                                create_diffuse_alpha_map_setup,
                                create_normal_map_setup
                                )
from .util_functions import find_texture_path_by_name


def create_tcsm_lashes_setup(mat: Material) -> None:
    export_path = bpy.context.preferences.addons['FFUS'].preferences.export_path
    related_path_to_common_textures = r"BBQGame\Content\BBQGame\Characters\Common\Textures"

    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_Common_Eyelash_AlphaOnly')

    texture = load_image_to_blender(path_to_texture)

    lashes_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    lashes_texture_node.location = (-424, 736)
    lashes_texture_node.name = 'Eyes Lashes texture'
    
    load_image_to_node(lashes_texture_node, texture, color_space='non_color')

    # create link lashes_texture_node[Color]-> principled_node[Alpha]
    principled_node = mat.node_tree.nodes['Principled BSDF']
    mat.node_tree.links.new(lashes_texture_node.outputs['Color'], principled_node.inputs['Alpha'])
    
    # black color to Base Color in Principled node
    principled_node.inputs['Base Color'].default_value = (0, 0, 0, 1)

    mat.blend_method = 'CLIP'


def create_tcsm_eye_transition_setup(mat: Material) -> None:
    export_path = bpy.context.preferences.addons['FFUS'].preferences.export_path
    related_path_to_common_textures = r"BBQGame\Content\BBQGame\Characters\Common\Textures"


    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_Common_Eyes_Transition_D')
    texture = load_image_to_blender(path_to_texture)
    create_diffuse_alpha_map_setup(mat, texture)

    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_Common_Eyes_Transition_N')
    texture = load_image_to_blender(path_to_texture)
    create_normal_map_setup(mat, texture)


def create_tcsm_eye_occlusion_setup(mat: Material) -> None:
    # material settings for eevee
    mat.surface_render_method = 'DITHERED'
    mat.use_raytrace_refraction = True

    principled_node = mat.node_tree.nodes["Principled BSDF"]
    principled_node.inputs['Alpha'].default_value = 1
    principled_node.inputs[18].default_value = 1
    principled_node.inputs['Roughness'].default_value = 0
    principled_node.inputs['IOR'].default_value = 1.333

    # value node
    value_node = mat.node_tree.nodes.new(type="ShaderNodeValue")
    value_node.location = (300, 128)

    value_node.outputs['Value'].default_value = 0.001

    # links
    material_output_node = mat.node_tree.nodes["Material Output"]
    mat.node_tree.links.new(value_node.outputs['Value'], material_output_node.inputs['Thickness'])


def create_tcsm_eye_iris_setup(mat: Material, iris_bc_texture_path: str) -> None:
    # clear all previous nodes and create empty principled setup
    create_empty_principled_bsdf_setup(mat)


    # set principled params:
    principled_node = mat.node_tree.nodes["Principled BSDF"]
    principled_node.inputs['Roughness'].default_value = 1

    # create texture node coordinate:
    texture_coordinate_node = mat.node_tree.nodes.new(type="ShaderNodeTexCoord")
    texture_coordinate_node.location = (-1960, 80)


    # bc_mapping_node
    bc_mapping_node = mat.node_tree.nodes.new(type="ShaderNodeMapping")
    bc_mapping_node.name = 'BC Mapping'
    bc_mapping_node.location = (-1660, 240)
    
    bc_mapping_node.inputs['Location'].default_value[0] = -1.25
    bc_mapping_node.inputs['Location'].default_value[1] = -1.25

    bc_mapping_node.inputs['Scale'].default_value[0] = 3.5
    bc_mapping_node.inputs['Scale'].default_value[1] = 3.5


    # normal_mapping_node
    normal_mapping_node = mat.node_tree.nodes.new(type="ShaderNodeMapping")
    normal_mapping_node.name = 'Normal Map Mapping'
    normal_mapping_node.location = (-1660, -180)

    normal_mapping_node.inputs['Location'].default_value[0] = -0.75
    normal_mapping_node.inputs['Location'].default_value[1] = -0.75

    normal_mapping_node.inputs['Scale'].default_value[0] = 2.5
    normal_mapping_node.inputs['Scale'].default_value[1] = 2.5

    # create links Texture coordinates[UV] -> bc_mapping_node[Vector]
    mat.node_tree.links.new(texture_coordinate_node.outputs['UV'], bc_mapping_node.inputs['Vector'])

    # create links Texture coordinates[UV] -> normal_mapping_node[Vector]
    mat.node_tree.links.new(texture_coordinate_node.outputs['UV'], normal_mapping_node.inputs['Vector'])


    # create texture and add images to them

    # iris_bc_texture_node
    iris_bc_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    iris_bc_texture_node.location = (-1350, 700)
    iris_bc_texture_node.name = 'Iris BC texture'
   
   # load iris BC texture
    texture = load_image_to_blender(iris_bc_texture_path)
    load_image_to_node(iris_bc_texture_node, texture)

    # create links bc_mapping_node[Vector] -> iris_bc_texture_node[Vector]
    mat.node_tree.links.new(bc_mapping_node.outputs['Vector'], iris_bc_texture_node.inputs['Vector'])



    # sclera_bc_texture_node
    sclera_bc_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    sclera_bc_texture_node.location = (-1350, 380)
    sclera_bc_texture_node.name = 'Sclera BC texture'


    export_path = bpy.context.preferences.addons['FFUS'].preferences.export_path
    related_path_to_common_textures = r"BBQGame\Content\BBQGame\Characters\Common\Textures"
    
    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_Common_Eyes_Sclera_A_D')
    texture = load_image_to_blender(path_to_texture)
    load_image_to_node(sclera_bc_texture_node, texture)


    # mask_node
    mask_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    mask_node.location = (-1350, 40)
    mask_node.name = 'Mask Texture'

    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_EyeMidPlaneDisplacement')
    texture = load_image_to_blender(path_to_texture)
    load_image_to_node(mask_node, texture, color_space='non_color')

    mask_node.extension = 'EXTEND'

    # create links normal_mapping_node[Vector] -> mask_node[Vector]
    mat.node_tree.links.new(normal_mapping_node.outputs['Vector'], mask_node.inputs['Vector'])


    # bc_mix_node
    bc_mix_node = mat.node_tree.nodes.new(type="ShaderNodeMix")
    bc_mix_node.location = (-660, 360)
    bc_mix_node.name = 'BC mix'
    bc_mix_node.data_type = 'RGBA'

    # create link iris_bc_texture_node[Color] -> bc_mix_node[B]
    mat.node_tree.links.new(iris_bc_texture_node.outputs['Color'], bc_mix_node.inputs['B'])

    # create link sclera_bc_texture_node[Color] -> bc_mix_node[A]
    mat.node_tree.links.new(sclera_bc_texture_node.outputs['Color'], bc_mix_node.inputs['A'])

    # create link mask_node[Color] -> mask_node[Factor]
    mat.node_tree.links.new(mask_node.outputs['Color'], bc_mix_node.inputs['Factor'])

    # create link bc_mix_node[Result] -> principled_node[Base Color]
    mat.node_tree.links.new(bc_mix_node.outputs['Result'], principled_node.inputs['Base Color'])


    # eye_normal_node
    eye_normal_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    eye_normal_node.location = (-1350, -300)
    eye_normal_node.name = 'Eye Normal Texture'


    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_EpicEye_N')
    texture = load_image_to_blender(path_to_texture)
    load_image_to_node(eye_normal_node, texture, color_space='non_color')


    
    # iris_normal_node
    iris_normal_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    iris_normal_node.location = (-1350, -650)
    iris_normal_node.name = 'Eye Iris Normal Texture'

    path_to_texture = find_texture_path_by_name((Path(export_path) / related_path_to_common_textures).iterdir(),  'T_Common_Iris_N')
    texture = load_image_to_blender(path_to_texture)
    load_image_to_node(iris_normal_node, texture, color_space='non_color')


    # create link bc_mapping[Vector] -> iris_normal_node[Vector]
    mat.node_tree.links.new(bc_mapping_node.outputs['Vector'], iris_normal_node.inputs['Vector'])

    # normal_mix_node
    normal_mix_node = mat.node_tree.nodes.new(type="ShaderNodeMix")
    normal_mix_node.location = (-960, -160)
    normal_mix_node.name = 'Normal Map mix'
    normal_mix_node.data_type = 'RGBA'

    # create link mask_node[Color] -> normal_mix_node[Factor]
    mat.node_tree.links.new(mask_node.outputs['Color'], normal_mix_node.inputs['Factor'])

    # create link eye_normal_node[Color] -> normal_mix_node[A]
    mat.node_tree.links.new(eye_normal_node.outputs['Color'], normal_mix_node.inputs['A'])

    # create link iris_normal_node[Color] -> normal_mix_node[B]
    mat.node_tree.links.new(iris_normal_node.outputs['Color'], normal_mix_node.inputs['B'])


    # separate_color_node
    separate_color_node = mat.node_tree.nodes.new(type="ShaderNodeSeparateColor")
    separate_color_node.location = (-780, -180)
    separate_color_node.name = 'Separate Color'

    # create link normal_mix_node[Result] -> separate_color_node[Color]
    mat.node_tree.links.new(normal_mix_node.outputs['Result'], separate_color_node.inputs['Color'])


    # invert_color_node
    invert_color_node = mat.node_tree.nodes.new(type="ShaderNodeInvert")
    invert_color_node.location = (-600, -210)
    invert_color_node.name = 'Invert Color'

    # create link separate_color_node[Green] -> invert_color_node[Color]
    mat.node_tree.links.new(separate_color_node.outputs['Green'], invert_color_node.inputs['Color'])



    # combine_color_node
    combine_color_node = mat.node_tree.nodes.new(type="ShaderNodeCombineColor")
    combine_color_node.location = (-420, -180)
    combine_color_node.name = 'Combine Color'

    # create link separate_color_node[Red] -> combine_color_node[Red]
    mat.node_tree.links.new(separate_color_node.outputs['Green'], combine_color_node.inputs['Red'])

    # create link separate_color_node[Blue] -> combine_color_node[Blue]
    mat.node_tree.links.new(separate_color_node.outputs['Blue'], combine_color_node.inputs['Blue'])

    # create link invert_color_node[Color] -> combine_color_node[Green]
    mat.node_tree.links.new(invert_color_node.outputs['Color'], combine_color_node.inputs['Green'])

    # normal_map_node
    normal_map_node = mat.node_tree.nodes.new(type="ShaderNodeNormalMap")
    normal_map_node.location = (-420, -180)
    normal_map_node.name = 'Normal Map'

    # create link combine_color_node[Color] -> normal_map_node[Color]
    mat.node_tree.links.new(combine_color_node.outputs['Color'], normal_map_node.inputs['Color'])

    # create link combine_color_node[Color] -> normal_map_node[Color]
    mat.node_tree.links.new(normal_map_node.outputs['Normal'], principled_node.inputs['Normal'])
