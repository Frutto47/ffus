from pathlib import Path

from typing import List


def find_texture_path_by_name(filepaths_list: List[Path], texture_name: str) -> str | None:
    for filepath in filepaths_list:
        if texture_name == filepath.stem:
            # texture file was found
            return str(filepath)
            