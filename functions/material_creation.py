import bpy
from bpy.types import Material, Image

from .image_functions import load_image_to_blender

from pathlib import Path


def create_empty_principled_bsdf_setup(mat: Material) -> None:
    # press 'Use Nodes' button
    mat.use_nodes = True

    # clear all nodes in material's node tree
    mat.node_tree.nodes.clear()

    # delete unused textures from blend file
    bpy.ops.outliner.orphans_purge(do_recursive=True)

    # create material output node
    material_output_node = mat.node_tree.nodes.new(type="ShaderNodeOutputMaterial")
    material_output_node.location = (300, 300)

    # create principled bsdf node
    principled_node = mat.node_tree.nodes.new(type="ShaderNodeBsdfPrincipled")
    principled_node.location = (-22, 300)

    # principled bsdf -> material output
    mat.node_tree.links.new(principled_node.outputs['BSDF'], material_output_node.inputs['Surface'])


def create_bc_setup(mat: Material, texture: Image) -> None:
    # create base texture node
    base_color_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    base_color_texture_node.location = (-1274, 769)
    base_color_texture_node.name = 'Base Color texture'
    base_color_texture_node.image = texture

    # base_color_texture_node[Base Color] -> principled_node[Base Color]
    principled_node = mat.node_tree.nodes['Principled BSDF']
    mat.node_tree.links.new(base_color_texture_node.outputs['Color'], principled_node.inputs['Base Color'])

    # add alpha from texture to Principled alpha, because mostly cases doesn't require it, but, some tcsm models does (alpha for Connie shorts)
    # base_color_texture_node[Alpha]  -> principled_node[Alpah]
    mat.node_tree.links.new(base_color_texture_node.outputs['Alpha'], principled_node.inputs['Alpha'])


def create_orm_setup(mat: Material, texture: Image) -> None:
    orm_nodes = []
    # create orm texture node
    orm_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    orm_texture_node.location = (-1274, 284)
    orm_texture_node.name = 'ORM texture'

    # change image type to Non Color
    texture.colorspace_settings.name = 'Non-Color'
    orm_texture_node.image = texture
    orm_nodes.append(orm_texture_node)

    # create separate color node for orm texture
    separate_color_orm_node = mat.node_tree.nodes.new(type="ShaderNodeSeparateColor")
    separate_color_orm_node.location = (-1004, 284)
    orm_nodes.append(separate_color_orm_node)

    # create roughness color ramp
    roughness_cr = mat.node_tree.nodes.new(type="ShaderNodeValToRGB")
    roughness_cr.location = (-826, 435)
    roughness_cr.label = 'Roughness'
    orm_nodes.append(roughness_cr)

    # create metallic color ramp
    metallic_cr = mat.node_tree.nodes.new(type="ShaderNodeValToRGB")
    metallic_cr.location = (-824, 214)
    metallic_cr.label = 'Metallic'
    orm_nodes.append(metallic_cr)


    # creating node linkls
    # orm color texture -> separate color orm
    mat.node_tree.links.new(orm_texture_node.outputs['Color'], separate_color_orm_node.inputs['Color'])

    # separate color orm -> roughness color ramp
    mat.node_tree.links.new(separate_color_orm_node.outputs['Green'], roughness_cr.inputs['Fac'])

    # separate color orm -> metallic color ramp
    mat.node_tree.links.new(separate_color_orm_node.outputs['Blue'], metallic_cr.inputs['Fac'])

    principled_node = mat.node_tree.nodes['Principled BSDF']

    # roughness color ramp -> principled roughness
    mat.node_tree.links.new(roughness_cr.outputs['Color'], principled_node.inputs['Roughness'])

    # metallic color ramp -> principled metallic
    mat.node_tree.links.new(metallic_cr.outputs['Color'], principled_node.inputs['Metallic'])


    # create node frame fop orm setup
    orm_frame_node = mat.node_tree.nodes.new(type='NodeFrame')
    # .name is for nodes dict key
    orm_frame_node.name = 'ORM'
     # .label is for UI in shader editor
    orm_frame_node.label = 'ORM (Occlusion Roughness Metallic)'
    orm_frame_node.use_custom_color = True
    orm_frame_node.color = (0.50, 0.60, 0.45)

    # make orm_nodes in frame
    for node in orm_nodes:
        node.parent = orm_frame_node


def create_normal_map_setup(mat: Material, texture: Image) -> None:
    normal_nodes = []
    # create normal texture node
    normal_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    normal_texture_node.location = (-1272, -76)
    normal_texture_node.name = 'Normal Map texture'
    normal_nodes.append(normal_texture_node)

    # create separate color node for normal texture
    separate_color_normal_node = mat.node_tree.nodes.new(type="ShaderNodeSeparateColor")
    separate_color_normal_node.location = (-1002, -76)
    normal_nodes.append(separate_color_normal_node)

    # create invert color node for normal texture
    invert_color_node = mat.node_tree.nodes.new(type="ShaderNodeInvert")
    invert_color_node.location = (-832.1, -76)
    normal_nodes.append(invert_color_node)

    # create combine color node for normal texture
    combine_color_node = mat.node_tree.nodes.new(type="ShaderNodeCombineColor")
    combine_color_node.location = (-662.1, -76)
    normal_nodes.append(combine_color_node)

    # create normal map node for normal texture
    normal_map_node = mat.node_tree.nodes.new(type="ShaderNodeNormalMap")
    normal_map_node.location = (-492, -76)
    normal_nodes.append(normal_map_node)

    texture.colorspace_settings.name = 'Non-Color'
    normal_texture_node.image = texture
    
    # normal color texture -> separate color normal texture
    mat.node_tree.links.new(normal_texture_node.outputs['Color'], separate_color_normal_node.inputs['Color'])

    # separate color normal texture red and blue -> combine color normal texture red and blue
    mat.node_tree.links.new(separate_color_normal_node.outputs['Red'], combine_color_node.inputs['Red'])
    mat.node_tree.links.new(separate_color_normal_node.outputs['Blue'], combine_color_node.inputs['Blue'])

    # separate color normal texture green -> invert color
    mat.node_tree.links.new(separate_color_normal_node.outputs['Green'], invert_color_node.inputs['Color'])

    # invert color -> combine color 
    mat.node_tree.links.new(invert_color_node.outputs['Color'], combine_color_node.inputs['Green'])

    # combine color -> normal map 
    mat.node_tree.links.new(combine_color_node.outputs['Color'], normal_map_node.inputs['Color'])

    principled_node = mat.node_tree.nodes['Principled BSDF']

    # normal map -> principled psdf
    mat.node_tree.links.new(normal_map_node.outputs['Normal'], principled_node.inputs['Normal'])


    # create node frame fop normal map setup
    normal_map_frame_node = mat.node_tree.nodes.new(type='NodeFrame')
    # .name is for nodes dict key
    normal_map_frame_node.name = 'Normal map frame'
     # .label is for UI in shader editor
    normal_map_frame_node.label = 'Normal Map'
    normal_map_frame_node.use_custom_color = True
    normal_map_frame_node.color = (0.31, 0.46, 0.60)

    # make normal_nodes in frame
    for node in normal_nodes:
        node.parent = normal_map_frame_node


def create_alpha_setup(mat: Material, texture: Image) -> None:
    # create alpha texture node
    alpha_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    alpha_texture_node.location = (-1274, 1067)
    alpha_texture_node.name = 'Alpha Map texture'

    texture.colorspace_settings.name = 'Non-Color'
    alpha_texture_node.image = texture
    
    # alpha texture -> principled alpha
    principled_node = mat.node_tree.nodes['Principled BSDF']
    mat.node_tree.links.new(alpha_texture_node.outputs['Color'], principled_node.inputs['Alpha'])


def create_bde_setup(mat: Material, texture: Image) -> None:
    bde_nodes = []
    # create bde texture node
    bde_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    bde_texture_node.location = (-1274, -435)
    bde_texture_node.name = 'BDE texture'

    texture.colorspace_settings.name = 'Non-Color'
    bde_texture_node.image = texture
    bde_nodes.append(bde_texture_node)
    
    
    # create separate color node for bde texture
    separate_color_bde_node = mat.node_tree.nodes.new(type="ShaderNodeSeparateColor")
    separate_color_bde_node.location = (-828, -435)
    bde_nodes.append(separate_color_bde_node)

    # create mix color node from separate color bde node
    mix_color_from_separate_color_node = mat.node_tree.nodes.new(type="ShaderNodeMix")
    mix_color_from_separate_color_node.location = (-658, -435)

    # set blend type to multiply
    mix_color_from_separate_color_node.blend_type = 'MULTIPLY'

    # set factor value to 1
    mix_color_from_separate_color_node.inputs['Factor'].default_value = 1
    bde_nodes.append(mix_color_from_separate_color_node)

    # set data type to color
    mix_color_from_separate_color_node.data_type = 'RGBA'

    # create second base color texture
    second_bc_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    second_bc_node.location = (-1274, -725)

    # use first bc node texture
    second_bc_node.image = mat.node_tree.nodes['Base Color texture'].image
    bde_nodes.append(second_bc_node)

    # create mix color node from second texture bc node
    mix_color_from_texture_bc_node = mat.node_tree.nodes.new(type="ShaderNodeMix")
    mix_color_from_texture_bc_node.location = (-840, -686)

    # set blend type to multiply
    mix_color_from_texture_bc_node.blend_type = 'MULTIPLY'

    # set factor value to 1
    mix_color_from_texture_bc_node.inputs['Factor'].default_value = 1
    bde_nodes.append(mix_color_from_texture_bc_node)

    # set data type to color
    mix_color_from_texture_bc_node.data_type = 'RGBA'

    
    # create value node
    value_node = mat.node_tree.nodes.new(type="ShaderNodeValue")
    # value of the node
    value_node.outputs['Value'].default_value = 30
    value_node.location = (-1000, -918)
    bde_nodes.append(value_node)

    # start creating links
    # bde texture -> bde separate color 
    mat.node_tree.links.new(bde_texture_node.outputs['Color'], separate_color_bde_node.inputs['Color'])

    # bde separate color -> mix_color_from_separate_color_node
    mat.node_tree.links.new(separate_color_bde_node.outputs['Blue'], mix_color_from_separate_color_node.inputs['A'])

    # bde separate color -> mix_color_from_separate_color_node
    mat.node_tree.links.new(separate_color_bde_node.outputs['Blue'], mix_color_from_separate_color_node.inputs['A'])

    # mix_color_from_separate_color_node -> principled bsdf
    principled_node = mat.node_tree.nodes['Principled BSDF']

    mat.node_tree.links.new(mix_color_from_separate_color_node.outputs['Result'], principled_node.inputs['Emission Color'])



    

    # second_bc_node -> mix_color_from_texture_bc_node
    mat.node_tree.links.new(second_bc_node.outputs['Color'], mix_color_from_texture_bc_node.inputs['A'])

    # value_node -> mix_color_from_texture_bc_node
    mat.node_tree.links.new(value_node.outputs['Value'], mix_color_from_texture_bc_node.inputs['B'])


    # mix_color_from_texture_bc_node -> mix_color_from_separate_color_node
    mat.node_tree.links.new(mix_color_from_texture_bc_node.outputs['Result'], mix_color_from_separate_color_node.inputs['B'])
    
    
    # create node frame fop normal map setup
    bde_frame_node = mat.node_tree.nodes.new(type='NodeFrame')
    # .name is for nodes dict key
    bde_frame_node.name = 'BDE frame'
     # .label is for UI in shader editor
    bde_frame_node.label = 'Emission'
    bde_frame_node.use_custom_color = True
    bde_frame_node.color = (0.60, 0.54, 0.45)

    # make bde_nodes in frame
    for node in bde_nodes:
        node.parent = bde_frame_node

    # set emission strength value
    principled_node.inputs['Emission Strength'].default_value = 20


def create_srtdm_setup(mat: Material, texture: Image) -> None:
    # SRTdM txture:
    # Red channel - specular
    # Green channel - roughness
    # Blue channel - subsurface scattering
    # Alpha channel - dM Detail Map 
    srtdm_nodes = []
    # create srtdm texture node
    srtdm_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    srtdm_texture_node.location = (-1274, 284)
    srtdm_texture_node.name = 'SRTdM texture'


    # change image type to Non Color
    texture.colorspace_settings.name = 'Non-Color'
    srtdm_texture_node.image = texture
    srtdm_nodes.append(srtdm_texture_node)

    # create separate color node for srtdm texture
    separate_color_srtdm_node = mat.node_tree.nodes.new(type="ShaderNodeSeparateColor")
    separate_color_srtdm_node.location = (-1004, 284)
    srtdm_nodes.append(separate_color_srtdm_node)


    # create roughness color ramp
    roughness_cr = mat.node_tree.nodes.new(type="ShaderNodeValToRGB")
    roughness_cr.location = (-826, 435)
    roughness_cr.label = 'Roughness'
    srtdm_nodes.append(roughness_cr)

    # create specular color ramp
    specular_cr = mat.node_tree.nodes.new(type="ShaderNodeValToRGB")
    specular_cr.location = (-824, 214)
    specular_cr.label = 'Specular'
    srtdm_nodes.append(specular_cr)


    # creating node linkls
    # srtdm color texture -> separate color srtdm
    mat.node_tree.links.new(srtdm_texture_node.outputs['Color'], separate_color_srtdm_node.inputs['Color'])

    # separate color srtdm Green channel -> roughness color ramp
    mat.node_tree.links.new(separate_color_srtdm_node.outputs['Green'], roughness_cr.inputs['Fac'])

    # separate color srtdm Red channel -> specular color ramp
    mat.node_tree.links.new(separate_color_srtdm_node.outputs['Red'], specular_cr.inputs['Fac'])


    principled_node = mat.node_tree.nodes['Principled BSDF']

    # roughness_cr -> Principled roughness
    mat.node_tree.links.new(roughness_cr.outputs['Color'], principled_node.inputs['Roughness'])

    # specular_cr -> Principled Specular IOR
    mat.node_tree.links.new(specular_cr.outputs['Color'], principled_node.inputs['Specular IOR Level'])

     # separate_color_srtdm_node Blue channel -> Principled Subsurface IOR
    mat.node_tree.links.new(separate_color_srtdm_node.outputs['Blue'], principled_node.inputs[8])

    # create node frame fop srtdm setup
    srtdm_frame_node = mat.node_tree.nodes.new(type='NodeFrame')
    # .name is for nodes dict key
    srtdm_frame_node.name = 'SRTDM'
     # .label is for UI in shader editor
    srtdm_frame_node.label = 'SRTdM (Specular Roughness Transluceny (Scattering) Detail Mask)'
    srtdm_frame_node.use_custom_color = True
    srtdm_frame_node.color = (0.50, 0.60, 0.45)

    # make srtdm_nodes in frame
    for node in srtdm_nodes:
        node.parent = srtdm_frame_node


def create_diffuse_alpha_map_setup(mat: Material, texture: Image) -> None:
    # create alpha texture node
    diffuse_alpha_texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
    diffuse_alpha_texture_node.location = (-1274, 769)
    diffuse_alpha_texture_node.name = 'Diffuse Alpha Map texture'

    diffuse_alpha_texture_node.image = texture
    
    # diffuse_alpha_texture_node  -> principled duffuse and alpha
    principled_node = mat.node_tree.nodes['Principled BSDF']
    mat.node_tree.links.new(diffuse_alpha_texture_node.outputs['Color'], principled_node.inputs['Base Color'])
    mat.node_tree.links.new(diffuse_alpha_texture_node.outputs['Alpha'], principled_node.inputs['Alpha'])

    mat.blend_method = 'CLIP'
