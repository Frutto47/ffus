import bpy
from bpy.types import Material

from .common_functions import create_empty_principled_bsdf_setup


def delete_unused_material(mat: Material) -> None:
    '''Delete all mesh parts, where unused material is assigned to'''

    # find all objects with this material
    objects_with_unused_material = set()
    for ob in (ob for ob in bpy.context.selected_objects if ob.type == 'MESH' and ob.material_slots):
        for mat_slot in ob.material_slots:
            if mat == mat_slot.material:
                objects_with_unused_material.add(ob)
    
    # deselect all objects
    bpy.ops.object.select_all(action='DESELECT')
   

    for ob in objects_with_unused_material:
        # make active object to be ob, so bpy.ops will only operate on this object
        bpy.context.view_layer.objects.active = ob
        ob.select_set(True)

        if bpy.context.mode != 'EDIT':
            bpy.ops.object.mode_set(mode='EDIT')
      

        for i in range(len(ob.material_slots)):
            if ob.material_slots[i].material == mat:
                material_slot_index = i
                break
        bpy.context.object.active_material_index = material_slot_index

        # deselect everything in edit mode
        bpy.ops.mesh.select_all(action='DESELECT')

        # change edit mode selection type to face
        bpy.ops.mesh.select_mode(type='FACE')

        # select mesh with current material assigned
        bpy.ops.object.material_slot_select()

        bpy.ops.mesh.delete(type='FACE')
        bpy.ops.object.mode_set(mode='OBJECT')
        ob.select_set(False)


def hide_unused_material(mat: Material) -> None:
    create_empty_principled_bsdf_setup(mat)
    mat.node_tree.nodes['Principled BSDF'].inputs['Alpha'].default_value = 0
    mat.blend_method = 'CLIP'
