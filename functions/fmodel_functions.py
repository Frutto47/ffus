from pathlib import Path
import json

from .util_functions import find_texture_path_by_name


def create_mat_filepath_fmodel_dict(export_path: str, materials_to_setup: set) -> dict:
    mat_filepath_dict = dict()
    # find all filepaths in export path with .json ending and matching materials names
    for filepath in Path(export_path).glob('**/*'):
        if not filepath.is_file():
            continue
        if filepath.suffix != '.json':
            continue
        for material in materials_to_setup:
            if filepath.stem == material.name:
                mat_filepath_dict[material] = filepath
    return mat_filepath_dict


def create_texture_type_path_fmodel_dict(mat_filepath: Path, export_path: Path, possible_textures: dict) -> (dict, dict):
    with open(mat_filepath) as file:
        material_json_dict = json.load(file)

    # define texture_type_path_dict dict
    # key - texture type, that was found in json file
    # value - absolute path to the texture
    texture_type_path_dict = dict()

    textures_not_found = dict()

    for json_texture_type, json_texture_path in material_json_dict['Textures'].items():

        # this is needed to make texture_paths dict with a key of common texture
        # so, if for example we encountered { Name=AORoughnessMetallic } texture, 
        # it will be transformed to texture_paths['ORM']
        # and not texture_paths['AORoughnessMetallic']

        # check if encountered any texture from possible_textures dict if not, then continue
        is_texture_found_in_possible_textures_dict = False
       
       # possible_textures is dict here
        for texture_type in possible_textures:
            if json_texture_type in possible_textures[texture_type]:
                current_texture_type = texture_type
                is_texture_found_in_possible_textures_dict = True
                break
            
        if not is_texture_found_in_possible_textures_dict:
            continue

        relative_path_to_texture = json_texture_path.split('.')[0]

        # relative path to texture is different in dbd and tcsm
        # in dbd fmodel ads '/' before the math
        # in tcsm fmodel does't ad '/' before the path
        # so the following condition is used to handle it

        if relative_path_to_texture.startswith('/'):
            relative_path_to_texture = Path(relative_path_to_texture[1:])
        else:
            relative_path_to_texture = Path(relative_path_to_texture)

        texture_name = relative_path_to_texture.stem
        folder_with_texture = export_path / relative_path_to_texture.parent
        

        from .common_functions import get_filepaths_in_folder
        from ..common_variables import filepaths_in_folder_dict


        filepaths_in_folder_dict[str(folder_with_texture)]: list = filepaths_in_folder_dict.get(str(folder_with_texture), get_filepaths_in_folder(folder_with_texture))

        texture_type_path_dict[current_texture_type] = find_texture_path_by_name(filepaths_in_folder_dict[str(folder_with_texture)], texture_name)

        
        if not texture_type_path_dict.get(current_texture_type):
            textures_not_found[f'{texture_type} {texture_name}'] = folder_with_texture
    return texture_type_path_dict, textures_not_found

