def is_material_present(context) -> bool:
    for ob in context.selected_objects:
        for mat_slot in ob.material_slots:
            # skip empty material slot
            if not mat_slot.name:
                continue
            return True
    return False


def selected_objects_materials_count(context) -> int:
    mat_count = 0
    for ob in context.selected_objects:
        for mat_slot in ob.material_slots:
            # skip empty material slot
            if not mat_slot.name:
                continue
            mat_count += 1
    return mat_count


def is_active_material_present(context) -> bool:
    if not context.active_object:
        return False
    if not context.active_object.active_material:
        return False
    if context.active_object.active_material.name:
        return True


def define_materials(context) -> tuple[set, set]:
    materials_to_setup = set()
    materials_to_skip = set()
    for obj in (ob for ob in context.selected_objects if ob.type == 'MESH'):
        for mat_slot in obj.material_slots:
            # skip empty slot
            if not mat_slot.name:
                continue
            mat_name = mat_slot.material.name
            if 'material' in mat_name.lower() or 'lambert' in mat_name.lower():
                materials_to_skip.add(mat_slot.material)
            materials_to_setup.add(mat_slot.material)
    return materials_to_setup, materials_to_skip
