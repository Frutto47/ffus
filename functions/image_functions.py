import bpy
from bpy.types import Image, Node


from pathlib import Path


def get_loaded_image(image_path: str) -> Image | None:
    for image in bpy.data.images:
        if image.filepath == image_path:
            return image


def load_image_to_blender(texture_path: str) -> Image | None:
    # path validation
    path_to_texture = Path(texture_path)
    if not (path_to_texture.is_file() and path_to_texture.exists()):
        return
    
    # check if image with same path is already loaded in blender
    image = get_loaded_image(texture_path)
    return image if image else bpy.data.images.load(texture_path)


def load_image_to_node(node: Node, image: Image, color_space='sRGB') -> None:
    node.image = image
    if color_space == 'non_color':
        image.colorspace_settings.name = 'Non-Color'
