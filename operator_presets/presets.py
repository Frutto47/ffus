import bpy

from bl_operators.presets import AddPresetBase
from bl_ui.utils import PresetPanel


class FFUS_MT_MyPresets(bpy.types.Menu):
    bl_label = 'My Presets'
    preset_subdir = 'FFUS'
    preset_operator = 'script.execute_preset'
    draw = bpy.types.Menu.draw_preset
    

class FFUS_OT_AddMyPreset(AddPresetBase, bpy.types.Operator):
    bl_idname = 'my.add_preset'
    bl_label = 'Add a new FFUS settings preset'
    bl_description = 'Add a new FFUS settings preset, using curring settings as a reference'
    preset_menu = 'FFUS_MT_MyPresets'

    # Common variable used for all preset values
    preset_defines = [  
        'props = bpy.context.scene.ffus_props',
        'prefs = bpy.context.preferences.addons["FFUS"].preferences'
    ]


    # Properties to store in the preset
    preset_values = [  
        'prefs.export_path',
        'prefs.log_folder_path',
        'props.export_program',
        'props.game_name',
        'props.action_on_unused_materials',
        'props.generate_log_file'
    ]

    # Directory to store the presets
    preset_subdir = 'FFUS'


class FFUS_PT_presets(PresetPanel, bpy.types.Panel):
    bl_label = 'FFUS Settings Presets'
    preset_subdir = 'FFUS'
    preset_operator = 'script.execute_preset'
    preset_add_operator = 'my.add_preset'


classes = [
    FFUS_MT_MyPresets,
    FFUS_OT_AddMyPreset,
    FFUS_PT_presets,
    ]

def register():
    for cl in classes:
        bpy.utils.register_class(cl)


def unregister():
    for cl in classes:
        bpy.utils.unregister_class(cl)
