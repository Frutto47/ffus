import bpy
from ..functions.operators_help_funcs import *
from ..functions.common_functions import ffus_ver_to_string

from ..addon_updater_ops import update_notice_box_ui
from ..operator_presets.presets import FFUS_PT_presets


from pathlib import Path


class Common_panel():
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'FFUS'
    bl_options = {'DEFAULT_CLOSED'}


class FFUS_PT_setup_materials(Common_panel, bpy.types.Panel):
    bl_label = f'Frutto Fast Unreal Setup v{ffus_ver_to_string()}'
    bl_options = set()


    def draw(self, context):
        props = context.scene.ffus_props

        addon_prefs = context.preferences.addons['FFUS'].preferences
        layout = self.layout

        box = layout.box()
        if not context.selected_objects:
            box.label(text='Select object', icon='ERROR')

        if context.selected_objects and not is_material_present(context):
            box.label(text='Object has no materials', icon='ERROR')

        export_path = Path(addon_prefs.export_path)
        if not export_path.exists() or not export_path.is_absolute():
            box.label(text='Export path doesn\'t exist', icon='ERROR')

        row = box.row()
        row.scale_y = 2
        if props.setup_only_active_material:
            text = 'Set Up Active Material'
        else:
            text = f'Set Up All Materials: {selected_objects_materials_count(context)}'
        row.operator("object.setup_materials", text=text, icon='MATSHADERBALL')

        box = layout.box()
        box.prop(props, 'setup_only_active_material')

        if props.setup_only_active_material:
            if is_active_material_present(context):
                box.label(text='Active material:')
                box.label(text=context.active_object.active_material.name)
            else:
                box.label(text='No active material')


        box = layout.box()
        if not context.selected_objects:
            box.label(text='Select object', icon='ERROR')

        row = box.row()
        row.scale_y = 1.25

        if props.clear_only_active_material:
            text = 'Clear Active Material'
        else:
            text = f'Clear All Materials: {selected_objects_materials_count(context)}'
        row.operator("object.clear_materials", text=text, icon='CANCEL')

        box.prop(props, 'clear_only_active_material')

        if props.clear_only_active_material:
            if is_active_material_present(context):
                box.label(text='Active material:')
                box.label(text=context.active_object.active_material.name)
            else:
                box.label(text='No active material')


class FFUS_PT_settings(Common_panel, bpy.types.Panel):
    bl_label = 'Settings'
    bl_parent_id = 'FFUS_PT_setup_materials'

    def draw_header_preset(self, _context):
        FFUS_PT_presets.draw_panel_header(self.layout)
        


    def draw(self, context):
        props = context.scene.ffus_props
        addon_prefs = context.preferences.addons['FFUS'].preferences
        layout = self.layout


        box = layout.box()
        box.label(text='Export folder path:')
        box.prop(addon_prefs, "export_path", text='')


        box = layout.box()
        box.label(text='Export program:')
        box.props_enum(props, "export_program")


        box = layout.box()
        box.label(text='Select game:')
        box.props_enum(props, "game_name")


        box = layout.box()
        box.label(text='Mesh with unused materials: ')
        box.props_enum(props, "action_on_unused_materials")


class FFUS_PT_debug_settings(Common_panel, bpy.types.Panel):
    bl_label = 'Debug settings'
    bl_parent_id = 'FFUS_PT_setup_materials'


    def draw(self, context):
        props = context.scene.ffus_props
        addon_prefs = context.preferences.addons['FFUS'].preferences
        layout = self.layout

        box = layout.box()
        box.prop(props, "generate_log_file")

        if props.generate_log_file:
            box.label(text='Log folder path:')
            box.prop(addon_prefs, "log_folder_path", text='')

            log_folder_path = Path(addon_prefs.log_folder_path)
            if not log_folder_path.is_absolute() or not log_folder_path.exists():
                box.label(text='Log folder path doesn\'t exist', icon='ERROR')
                box.label(text='Log file won\'t be created', icon='ERROR')


class FFUS_PT_info(Common_panel, bpy.types.Panel):
    bl_label = 'Info'


    def draw(self, context):
        layout = self.layout
        props = context.scene.ffus_props
        
        box = layout.box()
        row = box.row()
        row.alignment = 'CENTER'
        row.label(text=f'FFUS - FRUTTO FAST UNREAL SETUP', icon='INFO')

        row = box.row()
        row.label(text=f'FFUS Version: {ffus_ver_to_string()}', icon='LINENUMBERS_ON')

        row = box.row()
        row.scale_y = 1.5
        row.operator('wm.url_open', text='DBD Rendering discord server', icon='COMMUNITY').url = 'https://discord.gg/dk7BCD5W2u'

        row = box.row()
        row.scale_y = 1.5
        row.operator('wm.url_open', text='Frutto\'s discord server', icon='USER').url = 'https://discord.com/invite/EVtsJJh'

        update_notice_box_ui(self, context)


classes = [ FFUS_PT_setup_materials,
            FFUS_PT_settings,
            FFUS_PT_debug_settings,
            FFUS_PT_info
            ]


def register():
    for cl in classes:
        bpy.utils.register_class(cl)


def unregister():
    for cl in classes:
        bpy.utils.unregister_class(cl)
